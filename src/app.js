import http from './slhttp';
import ui from './ui';

//const idAuthor = "get_author";
//Events
document.addEventListener('DOMContentLoaded', fetchPosts);
document.getElementById('add_post_btn').addEventListener('click', addPost);
document.getElementById('posts').addEventListener('click', deletePost);
document.getElementById('posts').addEventListener('click', editPost);
document.getElementById('edit_post_btn').addEventListener('click', updatePostData);

document.getElementById('posts').addEventListener('click', getAuthors);


//Actual Functions

function fetchPosts(){
    http.get("http://localhost:4000/posts")
        .then(data => {
      ui.showPosts(data);
        console.log(data);
    } )
        .catch(err => console.log(err));
  
}


function getAuthors(e){
    e.preventDefault();
//    console.log(e.target);
    console.log(e.target.dataset.id);
        http.get(`http://localhost:4000/authors/${e.target.dataset.id}`)
            .then(data => ui.showAuthors(data))
            .catch(err => console.log(err))
    
}

//ui.getId(id, getAuthors);

function addPost(){
    const title = document.getElementById('post_title').value;
    const body = document.getElementById('post_body').value;
    const author = document.getElementById('post_author').value;
    
    const data = {
        title,
        body,
        author
    };
    
    http.post("http://localhost:4000/posts", data)
        .then(data => {
            ui.showAlert('Post created successfully!', 'alert alert-success');
            ui.clearFields();
            fetchPosts();
    })
        .catch(err => console.log(err));
}

function deletePost(e){
    e.preventDefault();
    console.log(e.target);
    if(e.target.classList.contains('delete')){
        const id = e.target.dataset.id;
        //console.log(id);
        if(confirm("Are you sure you want to delete the post?")){
            http.delete(`http://localhost:4000/posts/${id}`)
                .then(data => {
                    ui.showAlert("Post Deleted successfully!", "alert alert-success");
                    console.log(data);
                    fetchPosts();
            })
                .catch(err => console.log(err));
        }
    }
}


function editPost(e){
    e.preventDefault();
    if(e.target.classList.contains('edit')){
        const id = e.target.dataset.id;
        http.get(`http://localhost:4000/posts/${id}`)
            .then(post => ui.fillModalData(post))
            .catch(err => console.log(err));
    }
}


function updatePostData(){
    const title = document.getElementById('edit_post_title').value;
    const body = document.getElementById('edit_post_body').value;
    const author = document.getElementById('edit_post_author').value;
    
    const id    =   document.getElementById('edit_post_id').value;
    
    const data = {
        title,
        body,
        author
    };
    
    http.put(`http://localhost:4000/posts/${id}`, data)
        .then(data => {
            ui.showAlert('Post Edited successfully!', 'alert alert-success');
            fetchPosts();
    })
        .catch(err => console.log(err));
    
    //Closing the modal
    $('#editPost').modal('hide');
    
}












