//import app from './app'
class UI{
    constructor(){
        this.post_title     =   document.getElementById('post_title');
        this.post_body      =   document.getElementById('post_body');
        this.post_author    =   document.getElementById('post_author');

        

        this.edit_post_title     =   document.getElementById('edit_post_title');
        this.edit_post_body      =   document.getElementById('edit_post_body');
        this.edit_post_author    =   document.getElementById('edit_post_author');
        this.edit_post_id        =   document.getElementById('edit_post_id');
        
    
        this.author_description = document.getElementById('author_description');

        this.posts_wrapper  =   document.getElementById('posts');
        
        //submit button
        this.add_post_btn = document.getElementById('add_post_btn');
        
        /**
        *Post Container: This is the parent container where the forma and all post resides
        * posts_wrapper is child of Post Container
        */
        this.posts_container = document.querySelector('.posts-container');

    }
    
    showPosts(posts){
        let output = "";
        posts.forEach(post => {
            output += `
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">${post.title}</h5>
                        <p class="card-text">${post.body}</p>
                        <a href="#" class="card-link" data-id="${post.id}" data-toggle="modal" data-target="#author-details">${post.author}</a>
                        <a href="#" class="btn btn-primary pull-right mr-3 edit" data-id="${post.id}" data-toggle="modal" data-target="#editPost">Edit</a>
                        <a href="#" class="btn btn-danger pull-right mr-3 delete" data-id="${post.id}">Delete</a>
                    </div>
                </div>
            `;
        });
        this.posts_wrapper.innerHTML = output;
    }
    
//    getId(id, getName){
//        document.getElementById(id).addEventListener('click', getName);
//    }
    
    
    showAuthors(authors){
        let output = "";
        output += `
            <ul>
                <li>${authors.id}</li>
                <li>${authors.name}</li>
                <li>${authors.age}</li>
            </ul>
                         
        `;
        console.log(output);
        this.author_description.innerHTML = output;
    }

    showAlert(message, classList){
        //Create a div element
        const div = document.createElement('div');
        div.className = classList;
        div.appendChild(document.createTextNode(message));
        this.posts_container.insertBefore(div, this.posts_wrapper);
        console.log(div);
        setTimeout(() => {
            this.clearAlert();
        }, 3000);
    }

    clearAlert(){
        const currentAlertBox = document.querySelector('.alert');
        if(currentAlertBox){
            currentAlertBox.remove();
        }
    }

    clearFields(){
        this.post_author.value ="";
        this.post_body.value ="";
        this.post_title.value = "";
    }

    fillModalData(post){
        this.edit_post_author.value = post.author;
        this.edit_post_body.value = post.body;
        this.edit_post_title.value = post.title;
        this.edit_post_id.value = post.id;
    }
    
    fillAuthorData(){
        this.author_description.innerHTML = "Hey";
    }
}

const ui = new UI();
export default ui;